import { Component, OnInit } from '@angular/core';

import { User } from './models/user.model';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  user: User;
  cModal: boolean = true;

  constructor(
    private userService: UserService
  ) {
    const doc = (<any>window).document;
    let playerApiScript = doc.createElement('script');
    playerApiScript.type = 'text/javascript';
    playerApiScript.src = 'https://www.youtube.com/iframe_api';
    doc.body.appendChild(playerApiScript);

    const url = window.location;
  }

  ngOnInit() {
    this.user = this.userService.getUser();
  }

  allowCookies(bool: boolean) {
    this.userService.allowCookies(bool);
    if(!bool) this.cModal = false;
  }
}
