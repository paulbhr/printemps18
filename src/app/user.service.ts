import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, pipe } from "rxjs";
import { map } from "rxjs/operators";

import { User } from './models/user.model';

@Injectable()
export class UserService {
  user: User;

  constructor(private http: HttpClient) {
    if(localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    else {
      this.user = new User();
      localStorage.setItem('user', JSON.stringify(this.user));
    }
  }

  getUser(): User {
    return this.user;
  }

  getLastPlayed(): number {
    return this.user.lastPlayedID;
  }

  setLastPlayed(id: number): void {
    this.user.lastPlayedID = id;
    this.saveUser();
  }

  isShuffleAllowed(): boolean {
    return this.user.allowShuffle;
  }

  allowShuffle(): void {
    this.user.allowShuffle = true;
    this.saveUser();
  }

  isShuffleOn(): boolean {
    return this.user.shuffle;
  }

  setShuffle(bool: boolean): void {
    this.user.shuffle = bool;
    this.saveUser();
  }

  areCookiesAllowed(): boolean {
    return this.user.cookies;
  }

  allowCookies(bool: boolean): void {
    this.user.cookies = bool;
    this.saveUser();
  }

  saveUser(): void {
    if(this.user.cookies) localStorage.setItem('user', JSON.stringify(this.user));
  }
}
