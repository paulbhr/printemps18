import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, pipe } from "rxjs";
import { map } from "rxjs/operators";
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { Song } from '../models/song.model';

@Injectable()
export class PlayerService {
  playlistCollection: AngularFirestoreCollection<Song>;
  playlistObservable: Observable<Song[]>;

  constructor(
    private http: HttpClient,
    private db: AngularFirestore
  ) {
    this.playlistCollection = db.collection<Song>('songs', songs => songs.orderBy('position'));
    this.playlistObservable = this.playlistCollection.valueChanges();
  }

  getPlaylist(): Observable<Song[]> {
    return this.playlistObservable;
  }
}
