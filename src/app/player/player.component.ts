import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Observable, forkJoin, pipe } from "rxjs";
import { debounceTime, filter } from 'rxjs/operators';
const slugify = require('@sindresorhus/slugify');

import { AppComponent } from '../app.component';
import { User } from '../models/user.model';
import { PlayerService } from './player.service';
import { Song } from '../models/song.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
  user: User;
  playlist: Song[];
  player: any;
  autoplay: boolean;
  currentSong: Song = new Song();
  selectedTitle?: string;
  index: number = 0;
  interval: any;
  YT: any;
  video: any;
  state: string;
  url: string = window.location.origin;
  open: boolean;

  constructor(
    private playerService: PlayerService,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    this.route.params.subscribe(params => {
      if(params['title']) {
        this.selectedTitle = params['title'];
      }
    });
    this.autoplay = false;
    this.open = false;
    this.user = userService.getUser();
  }

  ngOnInit() {
    window['onYouTubeIframeAPIReady'] = (e) => {
      this.YT = window['YT'];
      this.player = new window['YT'].Player('ytplayer', {
        events: {
          'onReady': () => { this.loadPlaylist(); },
          'onStateChange': (ev) => {
            if(ev.data == 0) {
              this.nextSong();
            }
          }
        },
        playerVars: {
          fs: 0,
          playsinline: 1
        }
      });
    };
  }

  updateState(event: any) {
    this.state = event;
    switch(event) {
      case "PLAY": this.player.playVideo(); break;
      case "PAUSE": this.player.pauseVideo(); break;
      case "NEXT": this.nextSong(); break;
      default: this.player.pauseVideo(); break;
    }
  }

  getCurrentSong(playlist: Song[]): Song {
    this.currentSong = playlist[0];

    if(this.userService.getLastPlayed()) {
      this.currentSong = playlist[this.userService.getLastPlayed()];
    };

    playlist.forEach(element => {
      if(this.selectedTitle && slugify(element.title.toLowerCase()) == this.selectedTitle.toLowerCase()) {
        this.currentSong = element;
      }
    });
    this.playlist.splice(0, this.playlist.indexOf(this.currentSong)+1);
    return this.currentSong
  }

  cueSong() {
    this.player.cueVideoById(this.getCurrentSong(this.playlist).url);
  }

  loadPlaylist() {
    this.playerService.getPlaylist().subscribe(playlist => {
      this.playlist = playlist;
      if(this.userService.isShuffleOn()) {
        this.shufflePlaylist();
      }
      this.cueSong();
    });
  }

  nextSong() {
    if(this.playlist.length < 1) {
      this.userService.allowShuffle();
      this.loadPlaylist();
    }
    else {
      this.currentSong = this.playlist[0];
      this.userService.setLastPlayed(this.currentSong.position-1);
      this.autoplay = true;
      this.state = "PLAY";
      this.playlist.splice(0, 1);
      this.player.loadVideoById(this.currentSong.url);
    }
  }

  shufflePlaylist() {
    let ctr = this.playlist.length, temp, index;
    while (ctr > 0) {
        index = Math.floor(Math.random() * ctr);
        ctr--;
        temp = this.playlist[ctr];
        this.playlist[ctr] = this.playlist[index];
        this.playlist[index] = temp;
    }
  }

  allowShuffle(bool) {
    this.userService.setShuffle(bool);
    if(bool) this.shufflePlaylist();
  }

  slugURL(song: Song) {
    return `${this.url}/${slugify(song.title)}`;
  }

  playSong() {
    if(this.state == "PLAY") {
      this.player.pauseVideo();
      this.state = 'PAUSE';
    } else {
      this.player.playVideo();
      this.state = 'PLAY';
    }
  }

  copyLink(input: any) {
    input.select();
    document.execCommand('copy');
    input.setSelectionRange(0, 0);
    setTimeout(() => {this.open = !this.open}, 200);
  }
}
