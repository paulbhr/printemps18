import { Component, OnInit, AfterViewInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Observable, forkJoin, pipe } from "rxjs";
import { debounceTime, filter } from 'rxjs/operators';
import { detect }  from 'detect-browser';
const browser = detect();
const slugify = require('@sindresorhus/slugify');

import { Song } from '../models/song.model';

@Component({
  selector: 'app-interface',
  templateUrl: './interface.component.html',
  styleUrls: ['./interface.component.scss']
})
export class InterfaceComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() song: Song;
  @Input() state: string;
  url: string = window.location.origin;
  colors: string[] = ["#1940bc", "#b8336a", "#ffd151", "#03cea4"];
  colors0: string[] = ["#b8336a", "#ffd151", "#03cea4"];
  class: string[] = [".cls-5", "cls-3", "cls-2", "cls-4"];
  angle: number = 0;
  interval: any;

  yoloGroup: any;
  starGroup: any;
  flowers0: any;
  flowers1: any;
  flowers2: any;
  flowers2b: any;
  flower2: any;
  flower1: any;
  flower0: any;

  constructor() {}

  ngOnInit() {
    this.flowers0 = <any>document.querySelectorAll('#flower-0 > path');
    this.flowers1 = <any>document.querySelectorAll('#flower-1 > path');
    this.flowers2 = <any>document.querySelectorAll('#flower-2 > path');
    this.flowers2b = <any>document.querySelectorAll('#flower-2b path');
    this.yoloGroup = <any>document.querySelectorAll('#yolo-group g');
    this.starGroup = <any>document.querySelectorAll('#star-group g');
    this.flower2 = <any>document.querySelectorAll('#yolo-pattern');
    this.flower1 = <any>document.querySelectorAll('#flower-1');
    this.flower0 = <any>document.querySelectorAll('#flower-0');
  }

  ngAfterViewInit() {
    this.addColor();
    if(browser.name == 'ie') {
      alert('You may be using IE which is not supported.');
    }
  }

  ngOnChanges() {
    if(this.state == 'PLAY') {
      clearInterval(this.interval);
      this.interval = setInterval(() => {this.animate()}, (60000/(this.song.bpm)));
      window.addEventListener('focus', () => {
        clearInterval(this.interval);
        if(this.state == 'PLAY') this.interval = setInterval(() => {this.animate()}, (60000/(this.song.bpm)));
      });
    }
    else if(this.state == 'PAUSE' || this.state == 'STOP' || this.state == 'NEXT') {
      clearInterval(this.interval);
    }
  }

  addColor() {
    this.yoloGroup.forEach(group => {
      let nodes = group.childNodes;
      let random = Math.floor(Math.random() * (4));
      nodes.forEach(element => {
        element.style.stroke = this.colors[random];
        if(element.classList.contains('cls-14')) element.style.fill = this.colors[random];
      })
    })

    this.starGroup.forEach(group => {
      let nodes = group.childNodes;
      let random = Math.floor(Math.random() * (4));
      nodes.forEach(element => {
        element.style.stroke = this.colors[random];
      })
    })
  }

  changeColor() {
    this.shuffle(this.colors0);
    this.shuffle(this.colors);

    if(window.matchMedia('(orientation : landscape)').matches) {
      for(let x=0; x < this.colors0.length; x++) {
        this.flowers0[x].style.fill = this.colors0[x];
      }
      for(let x=0; x < this.colors.length; x++) {
        this.flowers1[x].style.fill = this.colors[x];
        this.flowers2[x].style.fill = this.colors[x];
      }
    }
    else {
      for(let x=0; x < this.colors0.length; x++) {
        this.flowers0[x+3].style.fill = this.colors0[x];
      }
      for(let x=0; x < this.colors.length; x++) {
        this.flowers1[x+4].style.fill = this.colors[x];
        this.flowers2[x+4].style.fill = this.colors[x];
      }
    }

    this.flowers2b.forEach(path => {
      path.classList.toggle('cls-2');
      path.classList.toggle('cls-3');
    })
  }

  rotate() {
    this.angle++;
    if(window.matchMedia('(orientation : landscape)').matches) {
      this.flower2[0].style.transform = `rotate(${this.angle}deg)`;
      this.flower0[0].style.transform = `rotate(${this.angle}deg)`;
      this.flower1[0].style.transform = `rotate(-${this.angle}deg)`;
    }
    else {
      this.flower2[1].style.transform = `rotate(${this.angle}deg)`;
      this.flower0[1].style.transform = `rotate(${this.angle}deg)`;
      this.flower1[1].style.transform = `rotate(-${this.angle}deg)`;
    }
  }

  animate() {
    this.addColor();
    this.changeColor();
    if(browser.name !== 'safari') this.rotate();
  }

  shuffle(array: any[]) {
    let ctr = array.length, temp, index;
    while (ctr > 0) {
        index = Math.floor(Math.random() * ctr);
        ctr--;
        temp = array[ctr];
        array[ctr] = array[index];
        array[index] = temp;
    }
    return array;
  }
}
