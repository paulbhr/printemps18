import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule }   from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import { ClipboardModule } from 'ngx-clipboard';

import { AppComponent } from './app.component';
import { PlayerComponent } from './player/player.component';
import { PlayerService } from './player/player.service';
import { InterfaceComponent } from './interface/interface.component';
import { UserService } from './user.service';
import { ModalComponent } from './modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    InterfaceComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    RouterModule.forRoot([
      {
        path: '',
        component: PlayerComponent
      },
      {
        path: ':title',
        component: PlayerComponent
      },
      {
        path: '**',
        component: PlayerComponent
      }
    ]),
    HttpClientModule
  ],
  providers: [
    PlayerService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
