export class Song {
  position?: number;
  artist?: string;
  title?: string;
  bpm?: number;
  duration?: number;
  url?: string;
  danceability?: number;
}
