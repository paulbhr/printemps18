export class User {
  lastPlayedID: number = 0;
  allowShuffle: boolean = false;
  shuffle: boolean = false;
  cookies: boolean = false;
}
