import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnChanges {
  @Input() header: string;
  @Input() state: boolean;
  @Output('state') emitter = new EventEmitter();

  constructor() {
  }

  ngOnChanges() {
  }

  toggleModal() {
    this.state = !this.state;
    this.emitter.emit(this.state);
  }

}
